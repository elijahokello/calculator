const express = require('express');
const cors = require("cors");
const client = require("prom-client");
const {Logging} = require("@google-cloud/logging");


async function quickstart(
  projectID = "hi-innovator-354112",
  logName = "my-log"
){
  const keyFileName = require("/Users/elijahokello/Desktop/calculator/calc/logging-key.json");
  //creates a client
  const logging = new Logging({projectID,keyFileName});

  //Selects the log to write to
  const log = logging.log(logName)

  // The data to write to the log
  const text = 'Hello World!';

  // The metadata associated with the entry
  const metadata = {
    resource:{type:'global'},
    severity: 'INFO'
  };

  // Prepares a log entry 
  const entry = log.entry(metadata, text);

  async function writeLog(){
    // Writes the log entry
    await log.write(entry);
    console.log(`Logged: ${text}`);
  }
  writeLog();
}

client.collectDefaultMetrics(); 

const app = express();
app.use(cors());


app.get("/", (req, res) => {
  // quickstart();
  res.send("This is a demo");
});

app.get('/metrics', async (_req, res) => {
  try{
    res.set('Content-Type',client.register.contentType);
    res.end(await client.register.metrics());
  }catch(err){
    res.status(500).end(err);
  }
});

app.listen(3000,()=>{
  console.log("Listening on 3000 !!!");
});
