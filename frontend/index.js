const express = require("express");
const cors = require("cors");
const axios = require("axios")

const app = express();
app.use(cors()); 

app.get("/hello",(req,res)=>{
  axios.get("http://calc-service:3000")
  .then(data => {
    console.log(data.data);
  })
  res.send(" K8s is awesome")

});

app.listen(3000,()=>{
  console.log("Listening on port 3000");
});